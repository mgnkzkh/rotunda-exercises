class Animal {
    constructor(avatar, sound) {
        this.avatar = avatar;
        this.sound = sound;
    }

    speak(frase) {
        this.frase = frase;

        let fraseReplaced = this.frase.replace(/\s|$/g, ` ` + this.sound + ` `);
        let fullFrase = this.avatar + ` ` + fraseReplaced;

        console.log(fullFrase);
    }
}

const lion = new Animal('🦁', 'roar');

const tiger = new Animal('🐯', 'grr');


console.log('🏯 🅉🄴🄽 🅉🄾🄾 ', '\n');

lion.speak("I'm a lion");
tiger.speak("Lions suck");
