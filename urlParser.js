let rawUrlFormat = '/:version/api/:collection/:id';
let rawUrl = '/6/api/listings/3?sort=desc&limit=10';
const regEx = /(:)/;

function urlParse(rawUrlFormat, rawUrl, regEx) {

    let urlFormat = rawUrlFormat.split(/[/]/);

    let url = rawUrl.split(/[?]/);
    let urlPath = url[0].split(/[/]/);
    let urlParams = url[1].split(/[&=]/);

    for (let i = 0; i < urlFormat.length; i++) {
        // Test for constant parts of URL and remove them.
        if (!regEx.test(urlFormat[i])) {
            urlFormat.splice(i, 1);
            urlPath.splice(i, 1);
        }

        // Remove colon from URL Format variable parts
        urlFormat[i] = urlFormat[i].replace(regEx, '');


        // Check for a valid number and convert it from string for desired output
        if (!isNaN(urlPath[i])) {
            urlPath[i] = Number(urlPath[i]);
        }
    }
    // Merge both arrays into an object as key:value pairs
    let hash = urlPath.reduce((hash, value, index) => {
        hash[urlFormat[index]] = value;
        return hash;
    }, {});

    // Slice URL Params and add them to object as key:value pairs   
    let chunk = 2;
    for (let i = 0; i < urlParams.length; i += chunk) {
        let s = urlParams.slice(i, i + chunk);
	// Check for a valid number and convert it from string for desired output
        if (!isNaN(s[1])) {
            s[1] = Number(s[1]);
        }
        hash[s[0]] = s[1];
    }

    return hash;
}

let hash = urlParse(rawUrlFormat, rawUrl, regEx);

console.log('🏯 🅉🄴🄽 🅄🅁🄻 🄿🄰🅁🅂🄴🅁', '\n')
console.log(hash);


